﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [SerializeField] private List<CinemachineVirtualCamera> cameras;
    public int index = 0;
    [SerializeField]
    GameObject car;

    void Start()
    {
        SetCamera();
    }

    void Update()
    {
    }


    void SetCamera()
    {
        // Set standard Cam
        if (index < 0 || index >= cameras.Count)
        {
            index = 0;
        }
        for (int i = 0; i < cameras.Count; i++)
        {
            if (i == index)
            {
                cameras[i].enabled = true;
            }
            else
            {
                cameras[i].enabled = false;
            }
        }
    }

    public void SetCarCamera()
    {
        index = (index + 1) % cameras.Count;
        SetCamera();
    }

}
