﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class AxisInfo
{
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;

    public bool motor;
    public bool steering; // know if this wheel can turn or not
}
public class CarController : MonoBehaviour
{
    [SerializeField]
    private List<AxisInfo> axisInfos;
    [SerializeField]
    private float maxMotorTorque; // traction
    [SerializeField]
    private float maxSteeringAngle;
    [SerializeField]
    private float maxBrake;
    public bool isActive = false;
    [SerializeField]
    GameObject garageDoor;
    [SerializeField]
    Image dialogUI;
    bool hasOpenDoor = false;

    void Start()
    {
        dialogUI.enabled = false;
        dialogUI.transform.GetChild(0).gameObject.SetActive(false);
    }

    public void ApplyLocalPositionVisuals(WheelCollider collider)
    {
        if (collider.transform.childCount == 0)
        {
            return;
        }
        Transform visualWheel = collider.transform.GetChild(0);

        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation); // La methode modifie les parametres et les retourne

        visualWheel.position = position;
        visualWheel.rotation = rotation;

    }

    void Update()
    {
        if (isActive)
        {
            if (!hasOpenDoor)
            {
                dialogUI.enabled = true;
                dialogUI.transform.GetChild(0).gameObject.SetActive(true);
            }
            else
            {
                dialogUI.enabled = false;
                dialogUI.transform.GetChild(0).gameObject.SetActive(false);
            }

            float motor = maxMotorTorque * Input.GetAxis("Vertical");
            float steering = maxSteeringAngle * Input.GetAxis("Horizontal");
            float brake = maxBrake * Input.GetAxis("Jump");


            foreach (AxisInfo axisInfo in axisInfos)
            {
                if (axisInfo.motor)
                {
                    axisInfo.leftWheel.motorTorque = motor;
                    axisInfo.rightWheel.motorTorque = motor;

                    axisInfo.leftWheel.brakeTorque = brake;
                    axisInfo.rightWheel.brakeTorque = brake;
                }
                if (axisInfo.steering)
                {
                    axisInfo.leftWheel.steerAngle = steering;
                    axisInfo.rightWheel.steerAngle = steering;
                }

                ApplyLocalPositionVisuals(axisInfo.leftWheel);
                ApplyLocalPositionVisuals(axisInfo.rightWheel);
            }

            if (Input.GetKey(KeyCode.Tab))
            {
                hasOpenDoor = true;
                garageDoor.GetComponent<Rigidbody>().isKinematic = false;
                garageDoor.GetComponent<HingeJoint>().useMotor = true;
            }
        }
        

    }
}
