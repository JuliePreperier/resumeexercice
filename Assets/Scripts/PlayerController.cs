﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    [SerializeField]
    float speed = 3;
    Rigidbody rb;
    [SerializeField]
    Image keyImg;
    [SerializeField]
    Image dialogUImg;
    [SerializeField]
    Image dialogUImgCar;
    public bool haskey = false;
    public bool isInCar = false;

    [SerializeField]
    CameraManager camera;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        keyImg.enabled = false;
        dialogUImg.enabled = false;
        dialogUImg.transform.GetChild(0).gameObject.SetActive(false);
        dialogUImgCar.enabled = false;
        dialogUImgCar.transform.GetChild(0).gameObject.SetActive(false);
    }

    void Update()
    {
        float x = Input.GetAxis("Horizontal") * speed;
        float z = Input.GetAxis("Vertical") * speed;
        rb.velocity = new Vector3(x, rb.velocity.y, z);
        
        if (rb.velocity.magnitude > 0.02f)
        {
            transform.LookAt(transform.position + rb.velocity);
        }

        if (haskey)
        {
            keyImg.enabled = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Key")
        {
            haskey = true;
            Destroy(other.gameObject);
        }
        if(other.tag == "Car")
        {
            dialogUImgCar.enabled = true;
            dialogUImgCar.transform.GetChild(0).gameObject.SetActive(true);
        }

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Car" && !isInCar && Input.GetKey(KeyCode.E))
        {
            Debug.Log("Enter the car");
            isInCar = true;
            other.GetComponent<CarController>().isActive = true;
            dialogUImgCar.enabled = false;
            dialogUImgCar.transform.GetChild(0).gameObject.SetActive(false);
            camera.SetCarCamera();
            gameObject.SetActive(false);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Door" && !haskey)
        {
            dialogUImg.enabled = true;
            dialogUImg.transform.GetChild(0).gameObject.SetActive(true);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Door" && !haskey)
        {
            dialogUImg.enabled = false;
            dialogUImg.transform.GetChild(0).gameObject.SetActive(false);
        }
    }
}
